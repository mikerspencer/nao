# ----------------------------------------------
# SSGB correlation map
# ----------------------------------------------

# Get longest complete year stations
dbSendQuery(SSGB.db, "
            CREATE temp TABLE step AS
            SELECT (HydroYear || ' ' || Station) AS Ind, HydroYear, Station, SnowlineElev FROM SSGB_obs
            WHERE
            strftime('%m', Date) BETWEEN '01' AND '05' AND SnowlineElev != 'n' AND SnowlineElev != '99'
            OR strftime('%m', Date) BETWEEN '10' AND '12'  AND SnowlineElev != 'n' AND SnowlineElev != '99'")

dbSendQuery(SSGB.db, "
            CREATE temp TABLE step1 AS
            SELECT HydroYear, Station FROM step
            WHERE Ind NOT IN
            (SELECT Ind FROM step WHERE SnowlineElev = 'm')
            GROUP BY HydroYear, Station")

SSGB.st = dbGetQuery(SSGB.db, "
                     SELECT Station, COUNT(Station) AS Years FROM step1
                     GROUP BY Station
                     ORDER BY Years DESC")

SSGB.all = merge(SSGB, SSGB.st)

SSGB.all.cor = by(SSGB.all, list(SSGB.all$Station, SSGB.all$elev), function(i){cor(x=i[,7], y=i[,3], method="pearson")})
SSGB.all.cor = stack(data.frame(rbind(SSGB.all.cor)))
SSGB.all.cor = data.frame(Station=sort(unique(SSGB.all$Station)), elev=SSGB.all.cor[,2], Correlation=round(SSGB.all.cor[,1], 2))

SSGB.all.cor = merge(SSGB.all.cor, loc, by.x="Station", by.y="KeyName")

SSGB.all.cor = merge(SSGB.all.cor, SSGB.st)

SSGB.all.sp = SpatialPointsDataFrame(SSGB.all.cor[,4:5], SSGB.all.cor[,c(3:6)])

cols = colorRampPalette(brewer.pal(6, "PiYG"))
cols = cols(10)[cut(SSGB.all.cor[SSGB.all.cor$elev=="High elevation", 3], breaks=10)]
cexs = 2 * (SSGB.all.cor[SSGB.all.cor$elev=="High elevation", 6] / max(SSGB.all.cor[SSGB.all.cor$elev=="High elevation", 6]))

# Map of correlations
png("./figures/SSGB_cor_map.png", width=1000, height=1200, res=140)
plot(dem, col=colorRampPalette(c("Black", "White"))(255), axis.args=list(cex.axis=0.8), legend.args=list(text="Elevation m AOD", side=4, line=2.5))
points(SSGB.all.sp[SSGB.all.cor$elev=="High elevation", ], col=cols, pch=19, cex=cexs)
dev.off()

# Scatter of correlation against record length
png("./figures/SSGB_cor_scatter.png", width=1000, height=1200, res=140)
par(mfrow=c(3,1))
plot(Correlation ~ Years, SSGB.all.cor[SSGB.all.cor$elev=="High elevation",], main="Above 600 m")
plot(Correlation ~ Years, SSGB.all.cor[SSGB.all.cor$elev=="Low elevation",], main="Below 600 m")
dev.off()