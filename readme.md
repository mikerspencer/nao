Read me file for NAO project
============================

Project to relate NAO phase to snow cover in Scotland.
------------------------------------------------------

**NAO Data taken from:**
http://www.cru.uea.ac.uk/~timo/datapages/naoi.htm
http://www.cru.uea.ac.uk/cru/data/nao/


**Snow cover data taken from:**
SSGB
Bonacina http://www.neforum2.co.uk/ferryhillweather/bonacina.html
Met Office UKCP09 http://www.metoffice.gov.uk/climatechange/science/monitoring/ukcp09/download/index.html
Station data: BADC MIDAS extract

Code in R

**Files:**
*Bonacina*
Detail relationship between Boncina index and NAO. Rmd (markdown) file superceeds plain R file.

*NAO_consistancy*
Are there trends in NAO? Can we forecast winter NAO from early winter NAO index?

*NAO_MOgrid*
Relationship between NAO and the MO UKCP09 snow lying grid.

*NAO_SSGB*
Relationship between NAO and SSGB for example stations.

The *_summary* files are written in markdown and should be treated like a report of the analyses undertaken. These should be read (preferably the html version) to follow the work.

*NAO_dataimport and NAO_functions*
Import most data required for the above analyses files and also contain the longer functions.
