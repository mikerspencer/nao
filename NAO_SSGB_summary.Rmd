NAO and SSGB correlation
========================================================

***Set working directory, import SSGB & NAO index and format NAO***
```{r setup}
setwd("~/Copy/Michael/Uni_temp/NAO/")
library(RSQLite)
source("repo/nao/NAO_functions.R")
source("repo/nao/NAO_dataimport.R")
```

***Prepare NAO***

Extract NAO for Jan '54 - May '04 (which matches SSGB period), group these into time periods (Oct-May and DJF). The mean index is calculated for Oct-May and DJF and a scatter comparison plotted below.
```{r prepare NAO}
NAO.SSGB = NAO[1597:2213,]
NAO.oc.ma = ext.mon(NAO.SSGB,10,17,50,12,FALSE)
NAO.oc.ma.mean = as.data.frame(cbind("Year"=1954:2004, gr.mon(NAO.oc.ma,1954:2004,10,5,8,"NAO")))
NAO.DJF = ext.mon(NAO.SSGB,12,14,50,12,FALSE)
NAO.DJF.mean = as.data.frame(cbind("Year"=1954:2004, gr.mon(NAO.DJF,1954:2004,12,2,3,"NAO")))
```
```{r plot NAO DJF vs Oct-May, fig.width=7, fig.height=6}
plot(NAO.oc.ma.mean$NAO.mean~NAO.DJF.mean$NAO.mean, main="NAO period comparison", xlab="Mean NAO pa, DJF", ylab="Mean NAO pa, Oct-May")
abline(0,1, lty=3)
```


***Prepare SSGB***

Extract SSGB stations to a list for group processing. The station elevations are as follows: ach_avie=300m, ard=130m, fer=260m and fort=0m. For ease, the station observations (when snow is lying at a station) are taken as 130 m, which adds the station observations to the 150 m snow line group. The elev.sum function also adds lower elevation days of snow cover to higher elevation days, giving total snow cover at each elevation.
```{r prepare SSGB}
# Station names
n = as.list(dbReadTable(SSGB.db, "SSGB_stations")[,1])
# Station data to a list (includes process timer)
ptm = proc.time()
SSGB.ext = lapply(n, function(i){
  dbGetQuery(SSGB.db, paste0("SELECT strftime('%Y-%m', Date), Snowline, COUNT(Snowline) FROM SSGB_obs WHERE Station=", "'", i, "'", "GROUP BY strftime('%Y %m', Date), Snowline HAVING strftime('%m', Date) BETWEEN '01' AND '05' OR strftime('%m', Date) BETWEEN '10' AND '12' ORDER BY Date"))
})
proc.time() - ptm
names(SSGB.ext) = n # Add station names
# Tidy column headings
SSGB.ext1 = lapply(SSGB.ext, function(i){
  colnames(i) = c("Date", "Snowline", "Count")
  return(i)})




SSGB.ext = lapply(SSGB.ext, function(i){elev.sum(i,130)})
# Group months into DJF
SSGB.DJF = lapply(SSGB.ext, function(i){
  ext.mon(i,3,5,50,8,FALSE)
})
# Sum SSGB oc to ma
SSGB.oc.ma.sum = lapply(SSGB.ext, function(x){
  as.data.frame(mapply(function(i)
  {tapply(i, x$Year, sum)
  }, x[7:14]))
})
# Sum SSGB DJF
SSGB.DJF.sum = lapply(SSGB.DJF, function(x){
  as.data.frame(mapply(function(i)
  {tapply(i, x$Year, sum)
  }, x[7:14]))
})
```

***Days snow cover per year by elevation***

Plots below shows the elevation on the x axis and snow cover (Oct-May) on the y axis for the example sites. Seems acceptable that Ardtalnaig gets less days snow cover than extreme west and east, as it will lie in a rain shaddow.
```{r plot all station SSGB vs elevation, fig.width=7, fig.height=6}
SSGB.mean.pa = lapply(SSGB.oc.ma.sum, function(i){
  colMeans(i, na.rm=T)})
SSGB.mean.pa = do.call(cbind.data.frame, SSGB.mean.pa)
SSGB.mean.pa$elevation = seq(150,1200,150)
plot(1, type="n", xlim=c(0,1200), ylim=c(0,200), xlab="Elevation (m AOD)", ylab="Days of snow cover pa")
#mapply(function(i){lines(x=i[5], y=i)}, SSGB.mean.pa)
lines(SSGB.mean.pa[,1]~SSGB.mean.pa[,5])
lines(SSGB.mean.pa[,2]~SSGB.mean.pa[,5], lty=2)
lines(SSGB.mean.pa[,3]~SSGB.mean.pa[,5], lty=3)
lines(SSGB.mean.pa[,4]~SSGB.mean.pa[,5], lty=4)
legend("topleft", legend=n, lty=1:4)
```

Extending the idea of the previous plot we can drill down for each site by year.
```{r plot station variation in SSGB vs elevation, fig.width=10, fig.height=10}
par(mfrow=c(2,2))
# add the plot and include median, quartiles and limits...
```

Following plots shows variation of SSGB snow cover through time.
```{r plot SSGB vs time, fig.width=10, fig.height=10}
par(mfrow=c(4,1))
year=as.numeric(rownames(SSGB.oc.ma.sum[[1]]))
#plot(SSGB.oc.ma.sum[[1]][,8]~year, ylab="Days of snow cover (Oct-May)")
for (i in seq(2,8,2)) {
  plot(SSGB.oc.ma.sum[[1]][,i]~year, main = names(SSGB.oc.ma.sum[[1]], ylab="Days snow cover, Oct-May")[i])
  abline(lm(SSGB.oc.ma.sum[[1]][,i]~year))
}
#points(SSGB.oc.ma.sum[[1]][,6]~year, pch=0)
#points(SSGB.oc.ma.sum[[1]][,4]~year, pch=2)
#points(SSGB.oc.ma.sum[[1]][,2]~year, pch=3)


# add the plot and include median, quartiles and limits...
```

***Correlation for given elevation to NAO***

By individual months (Oct-May) using pearson
```{r}
lapply(SSGB.ext, function(x){mapply(function(i){
  cor(x=NAO.oc.ma[2], y=i, method="pearson", use="complete.obs")
  }, x[7:14])
})
```
By individual months (Oct-May) using spearman
```{r}
lapply(SSGB.ext, function(x){
  mapply(function(i){
    cor(x=NAO.oc.ma[2], y=i, method="spearman", use="complete.obs")
  }, x[7:14])
})
```
By individual months (DJF) using pearson
```{r}
lapply(SSGB.DJF, function(x){
    mapply(function(i){
    cor(x=NAO.DJF[2], y=i, method="pearson", use="complete.obs")
  }, x[7:14])
})
```
By individual months (DJF) using spearman
```{r}
lapply(SSGB.DJF, function(x){
  mapply(function(i){
    cor(x=NAO.DJF[2], y=i, method="spearman", use="complete.obs")
  }, x[7:14])
})
```
By mean NAO (DJF) and summed SSGB (Oct-May) pearsons
```{r}
lapply(SSGB.oc.ma.sum, function(x){
  mapply(function(i){
    cor(x=NAO.DJF.mean[1], y=i, method="pearson", use='complete.obs')
  }, x[1:8])
})
```
By mean NAO (DJF) and summed SSGB (Oct-May) spearmans **This yields the best correlation**
```{r}
lapply(SSGB.oc.ma.sum, function(x){
  mapply(function(i){
    cor(x=NAO.DJF.mean[1], y=i, method="spearman", use='complete.obs')
  }, x[1:8])
}) # Yields the best correlation
```
Worth seeing best correlation as a scatter. Note the 150 m correlation is largely affected by having 0 values for the first 20 years of record.
```{r fig.width=8, fig.height=10}
par(mfrow=c(2,2))
# 1200 m Snow vs year
plot(SSGB.oc.ma.sum[[1]][,8]~NAO.DJF.mean[,1], main="Plot of ach_avie (1200 m) through time", xlab="Year", ylab="Total days snow cover during DJF")
abline(lm(SSGB.oc.ma.sum[[1]][,8]~NAO.DJF.mean[,1]))
# 150 m snow vs year
plot(SSGB.oc.ma.sum[[1]][,1]~NAO.DJF.mean[,1], main="Plot of ach_avie (150 m) through time", xlab="Year", ylab="Total days snow cover during DJF")
abline(lm(SSGB.oc.ma.sum[[1]][,1]~NAO.DJF.mean[,1]))
# 1200 m Snow vs year
plot(SSGB.oc.ma.sum[[1]][,8]~NAO.DJF.mean[,2], main="Plot of ach_avie (1200 m) through time", xlab="Year", ylab="Total days snow cover during DJF")
abline(lm(SSGB.oc.ma.sum[[1]][,8]~NAO.DJF.mean[,1]))
# 150 m snow vs year
plot(SSGB.oc.ma.sum[[1]][,1]~NAO.DJF.mean[,1], main="Plot of ach_avie (150 m) through time", xlab="Year", ylab="Total days snow cover during DJF")
abline(lm(SSGB.oc.ma.sum[[1]][,1]~NAO.DJF.mean[,1]))
```
By mean NAO (DJF) and summed SSGB (DJF) pearsons
```{r}
lapply(SSGB.DJF.sum, function(x){
  mapply(function(i){
    cor(x=NAO.DJF.mean[1], y=i, method="pearson", use='complete.obs')
  }, x[1:8])
})
```
By mean NAO (DJF) and summed SSGB (DJF) spearmans
```{r}
lapply(SSGB.DJF.sum, function(x){
  mapply(function(i){
    cor(x=NAO.DJF.mean[1], y=i, method="spearman", use='complete.obs')
  }, x[1:8])
})
```
By mean NAO (DJF) and individual SSGB months pearson
```{r}
SSGB.mon.NAO.mean.DFJ = lapply(SSGB.DJF, function(i){
  merge(NAO.DJF.mean, i)})
lapply(SSGB.mon.NAO.mean.DFJ, function(x){
  mapply(function(i){
    cor(x=x[2], y=i, method="pearson", use='complete.obs')
  }, x[8:15])
})
```
Strange result that stronger correlation inverts depending on if individual months or sum/mean months of NAO are considered. i.e. for ind months stronger correlation at lower elevations, for sum/mean months stronger correlations at higher elevations.


***ANOVA and Tukey post hoc***

Split NAO into groups of 0.5
```{r}
NAO.DJF.mean.gr = data.frame(NAO.DJF.mean$Year,cut(NAO.DJF.mean$NAO.mean, seq(-2.5,3,0.5)))
colnames(NAO.DJF.mean.gr)=c("Year","NAO")
```
Run and present the ANOVA
```{r}
SSGB.aov = lapply(SSGB.DJF.sum, function(x){
  mapply(function(i){aov(i ~ NAO.DJF.mean.gr[,2])}, x[,1:8])
})
summary(SSGB.aov)
# This code requires some effort to get working...
```

Split NAO into positive and negative and show box plots
```{r}
NAO.DJF.mean.hf = NAO.DJF.mean[,2] > 0
```
```{r fig.width=8, fig.height=6}
par(mfrow=c(2,4))
lapply
```
